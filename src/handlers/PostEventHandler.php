<?php namespace Sprocket\Blog\Handlers;

use \Input;
use \Sprocket\Blog\Repo\Tags;

class PostEventHandler {

	public function subscribe($events)
	{
		$events->listen('post.update', 'Sprocket\Blog\Handlers\PostEventHandler@onUpdate');
		$events->listen('post.store', 'Sprocket\Blog\Handlers\PostEventHandler@onStore');
		$events->listen('post.destroy', 'Sprocket\Blog\Handlers\PostEventHandler@onDestroy');
		$events->listen('post.*', 'Sprocket\Blog\Handlers\PostEventHandler@tags');
	}

	public function onUpdate($post)
	{
		// if (Input::hasFile('upload')) Image::uploadOriginalpost($post->id);
	}

	public function onStore($post)
	{
		// if (Input::hasFile('upload')) Image::uploadOriginalpost($post->id);
	}

	public function tags($post)
	{
		return;
		// if (Input::has('tags')) $post->tags()->sync(Tags::getIds());
		$post->tags()->sync($this->getIds());
	}

	public function onDestroy($g)
	{
		// $tmpl = "Post %s [%d] destroyed by %s";
		// Log::info(sprintf($tmpl, $g->title, $g->id, Auth::user()->email));
	}

	public function getIds()
	{
		$tags = explode(',', Input::get('tags'));
		$tag_ids = [];

		foreach ($tags as $tag) $tag_ids[] = Tags::findOrCreate($tag);

		return $tag_ids;
	}

}
