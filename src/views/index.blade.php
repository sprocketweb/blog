@extends('cms::layouts.main')

@section('header')
<div class="pull-right">
<a href="{{ action('Sprocket\Blog\CategoryController@index') }}" class="btn btn-default">Categories</a>
<a href="{{ action('Sprocket\Blog\PostController@create') }}" class="btn btn-info"><i class="fa fa-plus"></i> New Post</a>
</div>

<h1>All Posts</h1>
@stop


@section('content')
@if ($posts->count())

<form action="#" method="post">

<table class="table table-hover table-striped table-condensed table-bordered table-notebook">
<tbody>
@foreach($posts as $post)
<tr>
	<td>
		{{ Form::checkbox($post->id) }}
	</td>

	<td>
		<strong class="notebook-title">{{ $post->title }}</strong><br>
		<small>
			{{ Time::daysAgo($post->updated_at,0) }} in <strong>{{ $post->category->title }}</strong>
			by {{ $post->author->shortname }}
		</small>
	</td>
	<td>

	@foreach($post->tags as $tag)
	<span class="label label-default">{{ $tag->name }}</span>
	@endforeach
	</td>
	<td>
<div class="btn-group">
	<a href="http://{{ Config::get('cms::site.tld') }}/{{ $post->slug }}" class="btn btn-info btn-sm"><i class="fa fa-link"></i> Visit</a>
	<a href="{{ action('Sprocket\Blog\PostController@show',$post->id) }}" class="btn btn-info btn-sm posts-btn-preview"><i class="fa fa-eye"></i> Preview</a>
	<a href="{{ action('Sprocket\Blog\PostController@edit',$post->id) }}" class="btn btn-success btn-sm"><i class="fa fa-pencil"></i> Edit</a>
</div>

	</td>
</tr>
@endforeach
</tbody>
</table>

{{ Cms::totals('post', $posts->count(), 'footer-details') }}

@else
	<p class="lead">There are no posts.</p>
@endif
@include('blog::preview_modal')
@stop

@section('js')
<script>
$('.posts-btn-preview').on('click',function(e){
	e.preventDefault();
	$url = $(this).attr('href');
	var post_id = $url.split(/[//]+/).pop();
	$('#modal-btn-edit').attr('href','/admin/blog/'+post_id+'/edit');
	$('.modal .modal-body').load($url,
		function(e){
			$('.modal').modal({
				'show' : true,
				'backdrop' : true
			});
	});
});
</script>
@stop
