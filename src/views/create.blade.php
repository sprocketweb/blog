@extends('blog::layouts.main')

@section('header')
<h1>Create a new blog post</h1>
@stop

@section('css')
<link rel="stylesheet" href="/packages/sprocket/cms/vendor/redactor/redactor.css"/>
<link rel="stylesheet" href="/packages/sprocket/blog/selectize/css/selectize.bootstrap3.css"/>
@stop

@section('content')

@include('cms::pages.errors')

<div class="row col-md-10 col-md-offset-1">

{{ Form::open( [
	'url'=> URL::route('admin.blog.index'),
	'method' => 'post'
	]) }}

{{ Form::hidden('author_id', Auth::user()->id ) }}

@include('blog::form')

<section class="form-actions clearfix">
<a href="{{ URL::route('admin.blog.index') }}" class="btn btn-info btn-lg"><i class="fa fa-minus-circle"></i> Cancel</a>

<div class="btn-group pull-right">
	<button type="submit" class="btn btn-lg btn-warning" value="true" name="draft"><i class="fa fa-clock-o"></i> Save as Draft</button>
	<button type="submit" class="btn btn-lg btn-success" value="true" name="publish"><i class="fa fa-save"></i> Publish</button>
</div><!--btn-group-->

</section><!--form-actions-->

{{ Form::close() }}
</div>
@stop


@section('js')
<script src="/packages/sprocket/cms/vendor/redactor/redactor.min.js"></script>
@include('blog::js.edit')
@stop


