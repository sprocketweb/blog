<div class="row">

<div class="form-group col-lg-9">
	{{ Form::label('title') }}
	{{ Form::text('title',null,['class'=>'form-control']) }}
	<!-- <span class="help-block">Used on web browser windows and search engines results pages (max 70 characters).</span> -->
</div>

<div class="form-group col-lg-3">
	{{ Form::label('category_id', 'Category') }}
	{{ Form::select('category_id',$categories,null,['class'=>'form-control']) }}
</div>

</div><!--row-->

<div class="form-group">
	{{ Form::label('tags', 'Tags') }}
	<select id="tags" name="tags[]" multiple="multiple">
	  <option value="">Add tags to this post...</option>
	  @foreach($allTags as $tag)
	  <option value="{{ $tag }}" @if(isset($chosenTags) && in_array($tag,$chosenTags))selected="selected"@endif>{{ $tag }}</option>
	  @endforeach
	</select>
</div>


{{ Form::textarea('content',null,['class'=>'redactor']) }}

@if(Auth::user()->super)
<div class="row">
	<div class="form-group col-md-6">
		{{ Form::label('slug') }}
		<div class="input-group">
		  <span class="input-group-addon">{{ Config::get('cms::site.tld') }}/blog/{id}/</span>
		{{ Form::text('slug',null,['class'=>'form-control']) }}
		</div>
	</div>

	<div class="form-group col-md-3">
		{{ Form::label('visible') }}
		{{ Form::select('hidden',['Visible','Hidden'],null,['class'=>'form-control']) }}
	</div>

	<div class="form-group col-md-3">
		{{ Form::label('Author') }}
		{{ Form::UserSelect( isset($post->author_id)?$post->author_id:null, 'author_id') }}
	</div>
</div><!--row-->
@endif
