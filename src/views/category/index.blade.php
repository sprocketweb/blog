@extends('cms::layouts.main')

@section('header')
<div class="pull-right">
<a href="{{ action('Sprocket\Blog\PostController@index') }}" class="btn btn-default">Posts</a>
<a href="{{ action('Sprocket\Blog\CategoryController@create')}}" class="btn btn-info"><i class="fa fa-plus"></i> New Category</a>
</div>

<h1>All Categories</h1>
@stop

@section('content')
@if ($categories->count())

@foreach($categories as $category)
<div class="col-lg-4">
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">{{ $category->title }}</h3>
	</div>
	<div class="panel-body">
	{{ $category->description }}
	</div>
	<div class="panel-footer clearfix">
		<a href="{{ action('Sprocket\Blog\CategoryController@edit',$category->id) }}" class="btn btn-success pull-right"><i class="fa fa-pencil"></i> Edit</a>

		@if($category->id != 1)
		{{ Form::open( [
			'url'=> URL::route('admin.category.destroy',[$category->id]),
			'method' => 'delete',
			'class' => 'form-inline',
			'role' => 'form'
			]) }}
		<button class="btn btn-danger pull-right category-btn-delete pull-left"><i class="fa fa-exclamation-triangle"></i> Delete</button>
		{{ Form::close() }}
		@endif

	</div>
</div>
</div>
@endforeach

{{ Cms::totals('category', $category->count(), 'footer-details') }}


@else
	<p class="lead">There are no Categories.</p>
@endif

@stop

@section('js')
<script>
$('.category-btn-delete').on('click',function(e){
	return confirm('Are you sure you want to delete this category?');
});

</script>
@stop
