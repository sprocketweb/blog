<div class="row">

<div class="form-group col-lg-9 col-lg-offset-1">
	{{ Form::label('title') }}
	{{ Form::text('title',null,['class'=>'form-control']) }}
</div>

<div class="form-group col-lg-9 col-lg-offset-1">
	{{ Form::label('Description') }}
	{{ Form::textarea('description',null,['class'=>'form-control','rows'=>'6']) }}
</div>

@if(Auth::user()->super)
<div class="form-group col-md-8 col-lg-offset-1">
	{{ Form::label('slug') }}
	<div class="input-group">
	  <span class="input-group-addon">{{ Config::get('cms::site.tld') }}/blog/category/{id}/</span>
	{{ Form::text('slug',null,['class'=>'form-control']) }}
	</div>
</div>
@endif

</div><!--row-->
