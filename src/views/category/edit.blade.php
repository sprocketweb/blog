@extends('blog::layouts.main')

@section('header')
<h1>Edit: {{ $category->title }} </h1>
@stop

@section('css')
@stop


@section('content')

@include('cms::pages.errors')

<div class="row col-md-10 col-md-offset-1">

{{ Form::model( $category, [
	'url'=> action('Sprocket\Blog\CategoryController@update', $category->id),
	'method' => 'put',
	'id' => 'category-form-edit'
]) }}

@include('blog::category.form')

<section class="form-actions clearfix">
	<a href="{{ URL::route('admin.category.index') }}" class="btn btn-info btn-lg"><i class="fa fa-minus-circle"></i> Cancel</a>
	<button type="submit" class="btn btn-lg btn-success pull-right"><i class="fa fa-save"></i> Save Category</button>
</section><!--form-actions-->

{{ Form::close() }}
</div>
@stop

@section('js')
@stop
