<?php
// HTML button for super users
$buttons = CmsUser::isSuper() ? "'html'," :'';
$buttons .= Config::get('cms::editor.buttons');
?>

<script src="/packages/sprocket/blog/selectize/js/standalone/selectize.min.js"></script>
<script>
	// plugins: ['remove_button'],
$('#tags').selectize({
    plugins: ['restore_on_backspace','remove_button'],
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});

$('.redactor').redactor({
	focus: '{{ Config::get('cms::editor.focus',false) }}',
	fixed: '{{ Config::get('cms::editor.fixed') }}',
	toolbarFixedBox: '{{ Config::get('cms::editor.toolbarfixedbox') }}',
	toolbarFixedTopOffset: '{{ Config::get('cms::editor.toolbarfixedtopoffset') }}',
	iframe: '{{ Config::get('cms::editor.iframe') }}',
	css: '{{ Config::get('cms::editor.css') }}',

	imageUpload: '{{ Config::get('cms::image.url.upload') }}',
	imageGetJson: '{{ Config::get('cms::image.url.listing') }}',
	uploadFields: {'upload_dir' : '#upload_dir'},

	convertVideoLinks: '{{ Config::get('cms::editor.convertvideolinks') }}',
	convertImageLinks: '{{ Config::get('cms::editor.convertimagelinks') }}',
	linkAnchor: '{{ Config::get('cms::editor.linkanchor') }}',
	observeImages: '{{ Config::get('cms::editor.observeimages') }}',

	imageUploadErrorCallback: function(json)
	{
		// alert('There was a problem with your image: ' + json.error);
		alert('There was a problem with your image: ' + json.error );
	},
	buttons: [{{ $buttons }}],
	minHeight: '{{ Config::get('cms::editor.minheight') }}',
	autosave: {{ Config::get('cms::editor.autosave.use') }},
	interval: '{{ Config::get('cms::editor.autosave.interval') }}',
	autosaveCallback: autosave_cb,
	fileUpload: '{{ Config::get('cms::editor.file.upload') }}'
}); // redactor

function autosave_cb(data, redactor_obj){
	$('#redactor-autosave').html(data); // alert(data);
}
function image_error_cb(obj, response){
	// alert('There was an error uploading your image');
	alert(response.error);
}
</script>

