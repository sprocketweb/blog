<div class="col-sm-4">
<div class="panel panel-primary dashboard-panel">
  <div class="panel-heading">
    <h4 class="panel-title">
    <a href="{{ action('Sprocket\Blog\PostController@index') }}">
    Recent Posts</a></h4>
  </div>
  <div class="list-group">
  @foreach($posts as $post)
    <a href="{{ action('Sprocket\Blog\PostController@edit',$post->id) }}" class="list-group-item">
    <i class="fa fa-pencil pull-right"></i>
    {{ $post->title }}
    </a>
  @endforeach
  </div>
  <div class="panel-footer">

    <a href="{{ action('Sprocket\Blog\PostController@create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> </a>
  </div>
</div><!--panel-->

</div>
