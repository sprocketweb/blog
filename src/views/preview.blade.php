<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $post->title }}</title>
	{{ HTML::style("packages/sprocket/cms/vendor/bootstrap/css/bootstrap.min.css") }}
</head>
<body>
<h1>{{ $post->title }}</h1>
<p><small>Posted in <strong>{{ $post->category }}</strong> by {{ $post->author }}</small></p>
<hr>
{{ $post->content }}
</body>
</html>
