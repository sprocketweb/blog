@extends('blog::layouts.main')

@section('header')
<h1>Edit: {{ $post->title }} </h1>
@stop

@section('css')
<link rel="stylesheet" href="/packages/sprocket/cms/vendor/redactor/redactor.css"/>
<link rel="stylesheet" href="/packages/sprocket/blog/selectize/css/selectize.bootstrap3.css"/>
@stop


@section('content')

@include('cms::pages.errors')

<div class="row col-md-10 col-md-offset-1">


{{ Form::model( $post, [
	'url'=> action('Sprocket\Blog\PostController@update', $post->id),
	'method' => 'put',
	'id' => 'post-form-edit'
]) }}

{{ Form::hidden('author_id', $post->author_id) }}

@include('blog::form')

<section class="form-actions clearfix">
	<a href="{{ URL::route('admin.blog.index') }}" class="btn btn-info btn-lg"><i class="fa fa-minus-circle"></i> Cancel</a>
	<button type="submit" class="btn btn-lg btn-success pull-right"><i class="fa fa-save"></i> Save Post</button>
</section><!--form-actions-->

{{ Form::close() }}
</div>
@stop


@section('js')
<script src="/packages/sprocket/cms/vendor/redactor/redactor.min.js"></script>
@include('blog::js.edit')
@stop
