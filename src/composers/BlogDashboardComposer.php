<?php namespace Sprocket\Blog\Composers;

use Sprocket\Blog\Repo\PostRepo as Post;

class BlogDashboardComposer {

	/**
	 * blog posts
	 * @var collection
	 */
	protected $posts;

	public function __construct(Post $posts)
	{
		$this->post = $posts;
	}

	public function compose($view)
	{
		$posts = $this->post->getAll();
		// $posts = $this->post->getAll()->lists('title','id');

	    $view->with(compact('posts'));
	}

}
