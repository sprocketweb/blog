<?php namespace Sprocket\Blog\Composers;

use Sprocket\Blog\Repo\Category;

class PostComposer {

	/**
	 * blog categories
	 * @var collection
	 */
	protected $categories;

	public function __construct(Category $categories)
	{
		$this->categories = $categories;
	}

	public function compose($view)
	{
		$categories = $this->categories->all()->lists('title','id');

	    $view->with(compact('categories'));
	}

}
