<?php namespace Sprocket\Blog\Services\Validation;

use Sprocket\Cms\Services\Validation\Validator;

class CategoryValidator extends Validator {

	static $rules = [
			'title'			=> 'required',
			'description'	=> 'required',
			'slug'			=> 'sometimes|alpha_dash'
	];

}

