<?php namespace Sprocket\Blog\Services\Validation;

use Sprocket\Cms\Services\Validation\Validator;

class PostValidator extends Validator {

	static $rules = [
			'title'		=> 'required',
			'content'	=> 'required',
			'slug'		=> 'sometimes|alpha_dash',
			'author_id'	=> 'required'
	];

}

