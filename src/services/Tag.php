<?php namespace Sprocket\Blog\Services;

use \Config;
use \File;
use \Illuminate\Database\Eloquent\Collection;
use Sprocket\Blog\Repo\Tags\Tags as Tags;

class Tag {

	/**
	 * a basic list of tags as labels
	 * @param  array $tags
	 * @param  string $style the label style
	 * @return string        html list
	 */
	public function showTagsList($tags, $placeholder = false, $style = 'default')
	{
			if( ! count($tags))	return ($placeholder) ? '<small class="muted">no tags</small>' : '';

			$tmpl = "<span class=\"label label-$style\">%s</span> ";
			$html = '';

			foreach($tags as $tag) $html .= sprintf($tmpl, $tag->title);

			return $html;
	}

	public static function toSimpleList(Collection $tags, $seperator = ',')
	{
		if ( $tags == '' ) return;
		$tags = $tags->lists('title');
		$html = '';
		foreach ($tags as $tag => $title) {
			$html .= $title . $seperator;
		}
		return rtrim($html, $seperator);
	}


	public function showTagsSortList()
	{
		$tags = Tags::active();

		$tmpl = '<li class="filter" data-filter="%s">%s</li> ';
		$html = '<ul class="filter-list"><li class="filter" data-filter="all">all</li> ';

		foreach($tags as $tag) $html .= sprintf($tmpl, $tag->title, $tag->title);

		return $html . '</ul>';
	}

	/**
	 * a basic list of tags as labels
	 * @param  array $tags
	 * @param  string $style the label style
	 * @return string        html list
	 */
	public function showTagsLinks($tags)
	{
			if( ! count($tags))	return '';

			$tmpl = "<span class=\"label label-default\"><a href=\"/garments/%s\">%s</a></span> ";
			$html = '';

			foreach($tags as $tag) $html .= sprintf($tmpl,
				\Str::slug(strtolower($tag->title)),
				$tag->title
			);

			return $html;
	}

} // tag
