<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotPostTagTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('post_tag');

		Schema::create('post_tag', function($table)
		{
		    $table->increments('id');
		    $table->integer('tag_id')->unsigned();
		    $table->integer('post_id')->unsigned();
		    $table->timestamps();

		    $table->foreign('tag_id')
		          ->references('id')->on('tags')
		          ->onDelete('cascade');

		    $table->foreign('post_id')
		          ->references('id')->on('posts')
		          ->onDelete('cascade');
		});

	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('post_tag');
	}

}
