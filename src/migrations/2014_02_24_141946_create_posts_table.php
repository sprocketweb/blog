<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('posts');

		Schema::create('posts', function($table) {
			$table->increments('id');
			$table->integer('author',null);
			$table->integer('category',null);
			$table->boolean('hidden')->default(NULL)->nullable();
			$table->string('slug');
			$table->string('title');
			$table->text('content');
			$table->softDeletes();
			$table->timestamps();
		});
		// DB::statement('ALTER TABLE posts ADD FULLTEXT search(title, content)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
