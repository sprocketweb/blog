<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::dropIfExists('categories');

		Schema::create('categories', function($table) {
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->text('description')->nullable();
			$table->string('slug');
			$table->boolean('featured')->default(NULL)->nullable();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('categories');
	}

}
