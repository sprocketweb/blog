<?php namespace Sprocket\Blog;

use \Input;
use \Event;
use \Redirect;
use \Request;
use \View;
use \Sprocket\Blog\Repo\PostRepo as Post;
use \Sprocket\Blog\Repo\Category;
use \Sprocket\Blog\Services\Validation\CategoryValidator as Validator;

class CategoryController extends \Sprocket\Cms\BaseController {

	protected $category;
	protected $post;
	protected $validator;

	public function __construct
	(
		Post $post,
		Validator $validator,
		Category $category
	)
	{
		$this->category = $category;
		$this->post = $post;
		$this->validator = $validator;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = $this->category->getAll();

		return View::make('blog::category.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('blog::category.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if ( ! $this->validator->validate(Input::all()) ) return Redirect::back()->withErrors($this->validator->errors)->withInput();

		$category = $this->category->create(Input::all());

		if ($category->save()) return Redirect::route('admin.category.index')->with('message','The category was created');

		return Redirect::route('admin.category.index')->with('message','There was a problem saving your category.');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$category = $this->category->getById($id);

		return Request::ajax() ? $post->content : View::make('cms::posts.preview.index',compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$category = $this->category->getById($id);

		return View::make('blog::category.edit',compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		if ( ! $this->validator->validate($input) ) return Redirect::back()->withErrors($this->validator->errors)->withInput();

		$category = $this->category->getById($id);
		$category->fill($input)->save();

		Event::fire('category.update',$category);

		$msg = sprintf('The category "%s" has been updated.', $category->title);

		return Redirect::route('admin.category.index')->with('message',$msg);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		if ($id == 1 ) return Redirect::route('admin.category.index')->with('message', 'The General category cannot be deleted.');

		$this->category->find($id)->delete();

		return Redirect::route('admin.category.index')->with('message', 'The category was deleted.');
	}

}
