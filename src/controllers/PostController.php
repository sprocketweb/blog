<?php namespace Sprocket\Blog;

use \Input;
use \Event;
use \Redirect;
use \Request;
use \View;
use \Sprocket\Blog\Repo\PostRepo as Post;
use \Sprocket\Blog\Repo\TagRepo as Tag;
use \Sprocket\Blog\Repo\Category;
use \Sprocket\Blog\Services\Validation\PostValidator as Validator;


class PostController extends \Sprocket\Cms\BaseController {

	protected $categories;
	protected $post;
	protected $tag;
	protected $validator;

	public function __construct
	(
		Post $post,
		Validator $validator,
		Category $categories,
		Tag $tag
	)
	{
		$this->categories = $categories;
		$this->post = $post;
		$this->validator = $validator;
		$this->tag = $tag;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$posts = $this->post->getAll();

		return View::make('blog::index', compact('posts'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$allTags = $this->tag->getAll()->lists('name');

		return View::make('blog::create',compact('allTags'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if ( ! $this->validator->validate(Input::except('tags','publish','draft')) ) return Redirect::back()->withErrors($this->validator->errors)->withInput();

		if ($post = $this->createPost())
		{
			$tagIDs = $this->doTags();
			$post->tags()->sync($tagIDs);

			return Redirect::route('admin.blog.index')->with('message','The post was created');
		}

		return Redirect::route('admin.blog.index')->with('message','There was a problem saving your post.');
	}

	public function createPost()
	{
		$post_inputs = Input::except('tags','publish','draft');
		$post_inputs['hidden'] = Input::get('draft') ? 1 : 0;

		$post = $this->post->create($post_inputs);

		return $post->save() ? $post : false;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$post = $this->post->getById($id);

		return Request::ajax() ? $post->content : View::make('cms::posts.preview.index',compact('post'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$post = $this->post->getById($id);
		$allTags = $this->tag->getAll()->lists('name');
		$chosenTags = $post->tags->lists('name');

		return View::make('blog::edit',compact('post','chosenTags','allTags'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::except(['tags','publish']);
		if ( ! $this->validator->validate($input) ) return Redirect::back()->withErrors($this->validator->errors)->withInput();

		$post = $this->post->getById($id);

		$post->fill($input)->save();

		$tagIDs = $this->doTags();
		$post->tags()->sync($tagIDs);

		Event::fire('post.update',$post);

		$msg = sprintf('The post "%s" has been updated.', $post->title);

		return Redirect::route('admin.blog.index')->with('message',$msg);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function pivot()
	{
		$tag = $this->tag->first();
		$tag_id = $tag->id;

		$post = $this->post->getById(1);
		$post->tags()->attach($tag_id);

		return $post;
	}

	public function doTags()
	{
		$tagIDs = [];

		$tags = Input::get('tags');

		if ( empty($tags) ) return $tagIDs;

		foreach ($tags as $tag)
		{
			if( $existingTag = $this->tag->getByName($tag) ) $tagIDs[] = $existingTag->id;

			else {
				$newTag = new \Sprocket\Blog\Repo\Tag;
				$newTag->name = $tag;
				$newTag->save();

				$tagIDs[] = $newTag->id;
				}
		}

		return $tagIDs;
	}

}
