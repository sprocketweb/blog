<?php namespace Sprocket\Blog\Repo;

use \Eloquent;

class Tag extends Eloquent {

	protected $guarded = [];
	public $timestamps = false;

	public function save(array $options = [])
	{
		return parent::save($options);
	}

	public function posts()
	{
		return $this->belongsToMany('Sprocket\Blog\Repo\Posts','post_tag','post_id','tag_id');
	}

	public static function findOrCreate($tag)
	{
		$exists = self::where('title',trim(strtolower($tag)))->first();

		if ($exists) return $exists->toArray()['id'];

		$t = new Tag();
		$t->title = $tag;
		$t->save();

		return $t->id;
	}

	public static function getAll()
	{
		return self::all();
	}

}
