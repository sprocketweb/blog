<?php namespace Sprocket\Blog\Repo;

use Sprocket\Blog\Repo\Tag as Tag;
use Sprocket\Blog\Repo\TagRepositoryInterface;
use Sprocket\Cms\Repo\DbRepository;

class TagRepo extends DbRepository implements TagRepositoryInterface {

	protected $model;

	public function __construct(Tag $model)
	{
		$this->model = $model;
	}

	public function first()
	{
		return $this->model->first();
	}

	public function getByName($name)
	{
		return $this->model->where('name', $name)->first();
	}
}
