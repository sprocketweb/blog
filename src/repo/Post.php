<?php namespace Sprocket\Blog\Repo;

use \Eloquent;

class Post extends Eloquent {

	protected $guarded = array();

	protected $softDelete = true;

	public static function boot()
	{
	    parent::boot();
	}

	/* 'title','content','slug','menu','description','section','published','searchable','autosave','cacheable', */

	public function save(array $options = [])
	{
		return parent::save($options);
	}

	public function tags()
	{
		return $this->belongsToMany('Sprocket\Blog\Repo\Tag','post_tag','tag_id','post_id');
	}

	public function category()
	{
		return $this->belongsTo('Sprocket\Blog\Repo\Category','category_id');
	}

	public function author()
	{
		return $this->belongsTo('Sprocket\Blog\Repo\Author','author_id');
	}
}
