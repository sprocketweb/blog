<?php namespace Sprocket\Blog\Repo;

use \Eloquent;

class Category extends Eloquent {

	protected $guarded = [];
	public $timestamps = false;

	public static $rules = [
		//  FIXME: unique messes with updating the SLUG
		// 'title'			=> 'required|min:3|unique:categories,title',
		'title'			=> 'required|min:3',
		'description'	=> 'required|min:5'
		];

	public function getAll()
	{
		return Category::all();
	}

	public function posts()
	{
		return $this->hasMany('Sprocket\Blog\Repo\Posts');
	}

	public function getById($id)
	{
		return Category::find($id);
	}

}
