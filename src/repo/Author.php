<?php namespace Sprocket\Blog\Repo;

class Author extends \User {

	protected $table = 'users';

	public function posts()
	{
		return $this->hasMany('Sprocket\Blog\Repo\Post');
	}

}
