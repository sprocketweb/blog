<?php namespace Sprocket\Blog\Repo;

use Sprocket\Cms\Repo\DbRepository;
use Sprocket\Blog\Repo\Post as Post;
use Sprocket\Blog\Repo\PostsRepositoryInterface;

class PostRepo extends DbRepository implements PostRepositoryInterface {

	protected $model;

	public function __construct(Post $model)
	{
		$this->model = $model;
	}

	public function getAll()
	{
		return $this->model->with('tags')->with('category')->get();
		return $this->model->with('tags')->with('category')->with('author')->get();
	}

	public function getById($id)
	{
		return $this->model->with('tags')->with('category')->with('author')->find($id);
	}

}
