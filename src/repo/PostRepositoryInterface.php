<?php namespace Sprocket\Blog\Repo;

interface PostRepositoryInterface {

	public function getAll();

	public function getById($id);

	public function delete($id);

}
