<?php namespace Sprocket\Blog;

use \View;
// use Illuminate\Support\ServiceProvider;

class BlogServiceProvider extends BaseServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('sprocket/blog');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

		View::addNamespace('blog', __DIR__.'/../../views');

		// auto-include helpers
		$this->helperDir = __DIR__.'/../../helpers'; // helpers directory
		$this->includeHelpers();

		// other files
		$this->incl_files = [
			'handlers/handlers.php',
			'routes.php'
		];

		$this->srcDir = __DIR__.'/../../'; // package src directory
		$this->includeFiles();

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
