<?php namespace Sprocket\Blog;

use \View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

abstract class BaseServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * files to include
	 * outside of the Helper directory
	 * @var array files
	 */
	protected $incl_files = [];

	/**
	 * app source directory
	 * used for includes
	 * @var string path
	 */
	protected $srcDir = '';

	/**
	 * helper directory for autoloading
	 * @var string path
	 */
	protected $helperDir = '';

	/**
	 * aliases to add alias
	 * @var array
	 */
	protected $helperAliases = [];

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{

	}

/*
	public function boot()
	{
		$this->package('sprocket/cms');

		View::addNamespace('cms', __DIR__.'/../../views');

		// auto-include helpers
		$this->helperDir = __DIR__.'/../../helpers'; // helpers directory
		$this->includeHelpers();

		// other files
		$this->incl_files = [
			'handlers/handlers.php',
			'routes.php'
		];

		$this->srcDir = __DIR__.'/../../'; // package src directory
		$this->includeFiles();

	}
*/

/*
	public function register()
	{
		$this->helperAliases = ['Cms', 'CmsUser', 'Dev', 'Time'];

		$this->loadHelperAliases();
	}

*/


	/**
	 * include package files
	 * @return null
	 */
	protected function includeFiles()
	{
		foreach ($this->incl_files as $include)
		{
			require_once $this->srcDir . $include;
		}
	}

	/**
	 * include all helper files
	 * @return null
	 */
	protected function includeHelpers()
	{
	    foreach (glob("{$this->helperDir}/*.php") as $helper)
	    {
	        require_once $helper;
	    }
	}

	/**
	 * loads new aliases for helper files
	 * aliases, filenames, and classnames must be identical
	 * @return null
	 */
	protected function loadHelperAliases()
	{
		foreach ($this->helperAliases as $helper)
		{
			AliasLoader::getInstance()->alias($helper, "Sprocket\Cms\Helpers\\$helper");
		}
	}

}
