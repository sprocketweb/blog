<?php

Route::group(['prefix' => 'admin','before'=>'auth'], function()
{
	Route::resource('blog','Sprocket\Blog\PostController');

	Route::resource('category','Sprocket\Blog\CategoryController');

	Route::get('pivot',['uses'=>'Sprocket\Blog\PostController@pivot']);

});
